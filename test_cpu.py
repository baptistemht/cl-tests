import psutil
import time
import numpy as np

n=25000

print("--------------------------------------------------------------")
print("TEST : Addition de deux listes aléatoires de",n,"float32.")
print("--------------------------------------------------------------")
print("Plateforme : CPU")
print("Coeurs physiques : ", psutil.cpu_count(logical=False))
print("Coeurs logiques :", psutil.cpu_count(logical=True))
print("Fréquence : ", round(psutil.cpu_freq().current)/1000, "GHz")
print("--------------------------------------------------------------")

t0 = time.time()

a_np = np.random.rand(n).astype(np.float32)
b_np = np.random.rand(n).astype(np.float32)

res_np = [0]*len(a_np)

t1 = time.time()

for i in range(0, len(a_np)):
    res_np[i] = a_np + b_np

t2 = time.time()

print("Temps de setup (s) : ", round(t1-t0, 2))
print("Temps calcul (s) :",round(t2-t1, 2))
print("Temps total (s) : ", round(t2-t1, 2))
print("--------------------------------------------------------------")
"""
print("Résultats")
print(res_np - (a_np + b_np))
print(np.linalg.norm(res_np - (a_np + b_np)))

assert np.allclose(res_np, a_np + b_np)
print("--------------------------------------------------------------")
"""