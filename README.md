# Install OpenCL with clvk
This project is aimed to understand how to execute c code with opencl.

## Install clvk
Link to the repo is [here](https://github.com/kpet/clvk). It takes a lot of time on regular devices. We will need to look for cross-compiling options for arm based devices.

## Install Mesa Driver
On WSL, the only available device might be `llvmpipe` wich is CPU based. You need to install Mesa Drivers in WSL to access your dedicated GPU.
```
sudo add-apt-repository ppa:kisak/kisak-mesa
sudo apt update
sudo apt upgrade
```

## Compile & Run
`gcc -Wall -Wextra -D CL_TARGET_OPENCL_VERSION=100 Main.c -o HelloOpenCL -lOpenCL`

Otherwise, execute/debug the build from vscode.
CMake can be used.

## Tests
Don't forget to execute clvk tests to make sure its properly installed.

### Before Running

Install [OpenCL-ICD-Loader](https://github.com/KhronosGroup/OpenCL-ICD-Loader).

Then register clvk driver library on the device :

`echo full/path/to/clvk/build/libOpenCL.so > /etc/OpenCL/vendors/test.icd`

Follow example use of README file to integrate it with cmake.

## Python bindings
`pyopencl` is a python wrapper for the OpenCL SDK. It can be installed with `pip`.



## OpenCV link
`export OPENCV_OPENCL_DEVICE="clvk:"`

OpenCV is not yet supported by clvk. It might work in the future. Stay tuned on clvk repo.

## Kernels
Kernels can be written in OpenCL C or CPP (respectively `.cl` and `.clcpp` extensions). In order to be executed, it has to be converted into SPIR-V binary using `clspv` (compiled with clvk). `clspv` cannot compile `.clcpp`.

__Example use :__ 
`clspv test.cl -o test.spv`

More details [here](https://github.com/KhronosGroup/OpenCL-Guide/blob/main/chapters/os_tooling.md).

Those binaries can be called directly from any program having access to the opencl api.