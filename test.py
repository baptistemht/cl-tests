import time
import numpy as np
import pyopencl

n=25000
pl=pyopencl.get_platforms()[0].get_devices()[0]

print("--------------------------------------------------------------")
print("TEST : Addition de deux listes aléatoires de",n,"float32.")
print("--------------------------------------------------------------")
print("Plateforme : GPU")
print("Appareil : ", pl.hashable_model_and_version_identifier[3])
print("Driver :", pl.driver_version)
print("--------------------------------------------------------------")

t0 = time.time()

a_np = np.random.rand(n).astype(np.float32)
b_np = np.random.rand(n).astype(np.float32)

ctx = pyopencl.create_some_context()
queue = pyopencl.CommandQueue(ctx)

mf = pyopencl.mem_flags
a_g = pyopencl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
b_g = pyopencl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)

with open('test.cl', 'r') as il_file:
    il_buffer = ''.join(il_file.readlines())
prg = pyopencl.Program(ctx, il_buffer).build()

t1 = time.time()

res_g = pyopencl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
knl = prg.sum  # Use this Kernel object for repeated calls
knl(queue, a_np.shape, None, a_g, b_g, res_g)

res_np = np.empty_like(a_np)
pyopencl.enqueue_copy(queue, res_np, res_g)

t2 = time.time()

print("Temps de setup (s) : ", round(t1-t0, 2))
print("Temps calcul (s) :",round(t2-t1, 2))
print("Temps total (s) : ", round(t2-t1, 2))
print("--------------------------------------------------------------")
"""
print("Résultats")
print(res_np - (a_np + b_np))
print(np.linalg.norm(res_np - (a_np + b_np)))
assert np.allclose(res_np, a_np + b_np)
print("--------------------------------------------------------------")
"""