#include <stdio.h>

#include "CL/cl.h"

int main(){

    cl_int CL_err = CL_SUCCESS;
    cl_uint numPlatforms = 0;
    cl_int numDevices = 0;
    cl_platform_id* platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id)*1);
    cl_device_id* devices = (cl_device_id*) malloc(sizeof(cl_device_id)*2);
    cl_context ctx;
    cl_program prg;
    cl_kernel kernel;

    FILE *f;

    CL_err = clGetPlatformIDs(1, platforms, &numPlatforms);

    if(CL_err == CL_SUCCESS){
        printf("%u platform(s) found.\n", numPlatforms);

        CL_err = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, 2, devices, &numDevices);

        if(CL_err == CL_SUCCESS){
            printf("%u device(s) found.\n", numDevices);

            ctx = clCreateContext(NULL, 1, devices, NULL, NULL, &CL_err);

            if(CL_err == CL_SUCCESS){
                printf("Context created.\n");
                //create command queue then program then kernel
            }else{
                printf("clCreateContext(%i)\n", CL_err);
            }
        }else{
            printf("clGetDeviceIDs(%i)\n", CL_err);
        }

    }else{
        printf("clGetPlatformIDs(%i)\n", CL_err);
    }

    //clear everything

    free(platforms);
    free(devices);

    return 0;
}